package com.rahaman.asif.taskmanager.repositories;

import com.rahaman.asif.taskmanager.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<TaskEntity, Long> {
    List<TaskEntity> findByProject_Id(Long projectId);
}
