package com.rahaman.asif.taskmanager.controller;

import com.rahaman.asif.taskmanager.model.Task;
import com.rahaman.asif.taskmanager.model.TaskRequest;
import com.rahaman.asif.taskmanager.service.TaskService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    public TaskController(TaskService taskService){
        this.taskService = taskService;
    }

    @GetMapping
    public Page<Task> searchTask(){
        return taskService.searchTask();
    }

    @GetMapping("/taskId")
    public ResponseEntity<Task> getTaskDetails(@PathVariable Long taskId){
        return ResponseEntity.ok(taskService.getTaskDetails(taskId));
    }

    @PostMapping
    public ResponseEntity<Object> createTask(@Valid @RequestBody TaskRequest taskRequest){
        taskService.createTask(taskRequest);
        return ResponseEntity.ok().build();
    }

    @PutMapping("/taskId")
    public ResponseEntity<Task> updateTask(@PathVariable Long taskId, @Valid @RequestBody TaskRequest taskRequest){
        return ResponseEntity.ok(taskService.updateTask(taskId, taskRequest));
    }

}
