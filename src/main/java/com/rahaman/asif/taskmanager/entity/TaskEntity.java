package com.rahaman.asif.taskmanager.entity;

import com.rahaman.asif.taskmanager.model.Status;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Setter
@Getter
@EqualsAndHashCode
@Table(name = "tasks")
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "uid", unique = true)
    private Long uid;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id", nullable = false, updatable = false)
    private ProjectEntity project;

    @Column(name = "description", nullable = false, length = 512)
    private String description;

    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "due_date")
    private Date dueDate;

    @Column(name = "created")
    private Date created;
}
