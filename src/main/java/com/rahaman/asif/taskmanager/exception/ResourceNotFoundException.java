package com.rahaman.asif.taskmanager.exception;

public class ResourceNotFoundException  extends RuntimeException {

    public ResourceNotFoundException(String entityName, Long id) {
        super("Could not find " + entityName + ": " + id);
    }
}