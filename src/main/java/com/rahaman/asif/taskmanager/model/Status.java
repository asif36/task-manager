package com.rahaman.asif.taskmanager.model;

public enum Status {
    OPEN("open"),
    IN_PROGRESS("in progress"),
    CLOSED("closed");

    private String value;

    Status(String value){
        this.value = value;
    }

    public String getValue(){
        return this.value;
    }

}
