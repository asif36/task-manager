package com.rahaman.asif.taskmanager.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class TaskRequest {

    @NotBlank(message = "description can not be empty.")
    @Length(max = 512, message = "description should be within 512 characters.")
    private String description;

    @Pattern(regexp = "^open|closed|in progress$", message = "Status value should be open/closed/in progress.")
    private String status;

    @NotNull
    private Long projectId;

    private Date dueDate;
}
