package com.rahaman.asif.taskmanager.model;

import com.rahaman.asif.taskmanager.entity.ProjectEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Date;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Task implements Serializable {

    private Long uid;
    private ProjectEntity project;
    private String description;
    private String status;
    private Date dueDate;
    private Date created;

}
