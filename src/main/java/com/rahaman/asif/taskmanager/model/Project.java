package com.rahaman.asif.taskmanager.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode
@ToString
public class Project implements Serializable {

    private Long id;
    private String name;
    private Date created;
    private List<Task> taskList;
}
