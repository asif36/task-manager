package com.rahaman.asif.taskmanager.service;

import com.rahaman.asif.taskmanager.model.Task;
import com.rahaman.asif.taskmanager.model.TaskRequest;
import org.springframework.data.domain.Page;

public interface TaskService {
    Page<Task> searchTask();

    void createTask(TaskRequest taskRequest);

    Task updateTask(Long taskId, TaskRequest taskRequest);

    Task getTaskDetails(Long taskId);
}
