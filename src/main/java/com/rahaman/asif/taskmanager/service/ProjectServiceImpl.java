package com.rahaman.asif.taskmanager.service;

import com.rahaman.asif.taskmanager.entity.ProjectEntity;
import com.rahaman.asif.taskmanager.exception.ResourceNotFoundException;
import com.rahaman.asif.taskmanager.model.Project;
import com.rahaman.asif.taskmanager.model.ProjectRequest;
import com.rahaman.asif.taskmanager.repositories.ProjectRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;

    private final String FEATURE_NAME = "project";

    public ProjectServiceImpl(ProjectRepository projectRepository){
        this.projectRepository = projectRepository;
    }

    @Override
    public Page<Project> getProjects(Pageable pageable) {
        Page<ProjectEntity> projectEntityPage = projectRepository.findAll(pageable);
        List<Project> projectList = new ArrayList<>();
        for (ProjectEntity projectEntity: projectEntityPage) {
            projectList.add(convertEntityToResource(projectEntity));
        }
        return new PageImpl<>(projectList, pageable, projectEntityPage.getTotalElements());
    }

    @Override
    public void createProject(ProjectRequest projectRequest) {
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setName(projectRequest.getName());
        projectEntity.setCreated(new Date());
        projectRepository.save(projectEntity);
    }

    @Override
    public Project getProjectDetails(Long projectId) {
        return convertEntityToResource(getProject(projectId));
    }

    @Override
    public void deleteProject(Long projectId) {
        Optional<ProjectEntity> projectEntity = projectRepository.findById(projectId);
        if(!projectEntity.isPresent()){
            throw new ResourceNotFoundException(FEATURE_NAME, projectId);
        } else {
            projectRepository.delete(projectEntity.get());
        }
    }

    @Override
    public ProjectEntity getProject(Long projectId){
        Optional<ProjectEntity> projectEntity = projectRepository.findById(projectId);
        if(!projectEntity.isPresent()){
            throw new ResourceNotFoundException(FEATURE_NAME, projectId);
        }
        return projectEntity.get();
    }

    private Project convertEntityToResource(ProjectEntity projectEntity){
        Project project = new Project();
        project.setId(projectEntity.getId());
        project.setName(projectEntity.getName());
        project.setCreated(projectEntity.getCreated());
        return project;
    }
}
