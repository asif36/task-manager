package com.rahaman.asif.taskmanager.service;

import com.rahaman.asif.taskmanager.entity.ProjectEntity;
import com.rahaman.asif.taskmanager.model.Project;
import com.rahaman.asif.taskmanager.model.ProjectRequest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProjectService {
    Page<Project> getProjects(Pageable pageable);
    void createProject(ProjectRequest projectRequest);
    Project getProjectDetails(Long projectId);
    void deleteProject(Long projectId);
    ProjectEntity getProject(Long projectId);
}
