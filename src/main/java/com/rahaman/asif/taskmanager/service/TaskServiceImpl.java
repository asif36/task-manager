package com.rahaman.asif.taskmanager.service;

import com.rahaman.asif.taskmanager.entity.TaskEntity;
import com.rahaman.asif.taskmanager.exception.InvalidRequestException;
import com.rahaman.asif.taskmanager.exception.ResourceNotFoundException;
import com.rahaman.asif.taskmanager.model.Status;
import com.rahaman.asif.taskmanager.model.Task;
import com.rahaman.asif.taskmanager.model.TaskRequest;
import com.rahaman.asif.taskmanager.repositories.TaskRepository;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class TaskServiceImpl implements TaskService {

    private TaskRepository taskRepository;
    private ProjectService projectService;

    private final String CLOSED_STATUS_ERROR_MESSAGE = "Closed tasks cannot be edited.";
    private final String FEATURE_NAME = "task";

    public TaskServiceImpl(TaskRepository taskRepository, ProjectService projectService){
        this.taskRepository = taskRepository;
        this.projectService = projectService;
    }

    @Override
    public Page<Task> searchTask() {
        return null;
    }

    @Override
    public void createTask(TaskRequest taskRequest) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setDescription(taskRequest.getDescription());
        taskEntity.setProject(projectService.getProject(taskRequest.getProjectId()));
        taskEntity.setStatus(Status.valueOf(taskRequest.getStatus()));
        taskEntity.setDueDate(taskRequest.getDueDate());
        taskEntity.setCreated(new Date());
        taskRepository.save(taskEntity);
    }

    @Override
    public Task updateTask(Long taskId, TaskRequest taskRequest) {
        TaskEntity taskEntity = getTask(taskId);
        if(Status.CLOSED.equals(taskEntity.getStatus())){
            throw new InvalidRequestException(CLOSED_STATUS_ERROR_MESSAGE);
        }
        taskEntity.setDescription(taskRequest.getDescription());
        taskEntity.setStatus(Status.valueOf(taskRequest.getStatus()));
        taskEntity.setDueDate(taskRequest.getDueDate());
        TaskEntity updatedTaskEntity = taskRepository.save(taskEntity);
        return convertEntityToResource(updatedTaskEntity);
    }

    @Override
    public Task getTaskDetails(Long taskId) {
        return convertEntityToResource(getTask(taskId));
    }

    private TaskEntity getTask(Long taskId){
        Optional<TaskEntity> taskEntity = taskRepository.findById(taskId);
        if (!taskEntity.isPresent()){
            throw new ResourceNotFoundException(FEATURE_NAME, taskId);
        }
        return taskEntity.get();
    }

    private Task convertEntityToResource(TaskEntity taskEntity){
        Task task = new Task();
        task.setUid(taskEntity.getUid());
        task.setProject(taskEntity.getProject());
        task.setDescription(taskEntity.getDescription());
        task.setStatus(taskEntity.getStatus().getValue());
        task.setDueDate(taskEntity.getDueDate());
        task.setCreated(taskEntity.getCreated());
        return task;
    }
}
